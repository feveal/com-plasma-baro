��          �      ,      �     �     �     �     �  	   �  	   �  j   �     D  	   M     W     m     s          �  $   �     �  c  �     (     >  	   Z     d     v     �  c   �     �               )     6     F     W      d     �                                 	                           
                  @titleFont Appearance Degrees Units: Details: Font Family: Forecast: Humidity: Note: 'wetter.com' server does not provide the current temperature, for this use other servers such as BBC Outline: Pressure: Show weather warnings Temp: Text Color: Update every: Visibility: Weather Temperature in Old Barometer Wind: Project-Id-Version: baro_feveal
Report-Msgid-Bugs-To: -prueba
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 19.04.3
 Apariencia de Fuentes Ver Alertas Meteorológicas Detalles: Familia de Fuente Predicción: Humedad: Nota: El servidor 'wetter.com' no proporciona la temperatura actual. Utilice otro servidor como BBC Borde de carácter: Presión Ver Alertas Meteorológicas Temperatura: Color de Texto: Actualizar cada: Visibilidad: Temperatura en Barometro antiguo Viento: 