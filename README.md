com.plasma.baro

This Plasmoid provides weather forecast on the image of an old barometer. The needle shows the temperature. The image also appears with the
current prediction, temperature and other details. 

Getting started:
The Plasmoid is installed just like the rest. ".local/share/plasma/plasmoids/com.plasma.baro/"
Integrate with your tools. IMPORTANT, The ZIP adds "-main" it needs to be removed

Name:
com.plasma.baro, This Plasmoid provides weather forecast on the image of an old barometer. The needle shows the temperature. The image also
appears with the current prediction, temperature and other details. It also has an anemometer that rotates depending on the wind speed 

Description:
Plasmoid for KDE 

Authors and acknowledgment:
Fernando Velez (feveal@hotmail.com), but using routines from other authors ( xyz.relativity@gmail.com , zrenfire@gmail.com ) 

License:
For open source projects, say how it is licensed
